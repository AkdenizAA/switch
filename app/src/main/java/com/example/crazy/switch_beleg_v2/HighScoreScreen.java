package com.example.crazy.switch_beleg_v2;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class HighScoreScreen extends ActionBarActivity {

    List<String> highscoreList = new ArrayList<String>();
    int[] highscore;
    int topCounter1 = 0, topCounter2 = 0, topCounter3 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_score_screen);

        loadingData();
        setTopThree();

        final ListView lv = (ListView) (findViewById(R.id.HighScore));
        ListAdapter listAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, highscoreList);
        lv.setAdapter(listAdapter);


    }


    public void loadingData() {

        try {
            FileInputStream fileIn = openFileInput("highscore5.txt");
            InputStreamReader InputRead = new InputStreamReader(fileIn);

            char[] inputBuffer = new char[1024];
            int charRead;

            charRead = InputRead.read(inputBuffer);

            String readstring = String.copyValueOf(inputBuffer, 0, charRead);
            String[] parts = readstring.split("\r\n");
            highscore = new int [parts.length];
            for (int i = 1; i < parts.length; i++) {
                highscore[i] = Integer.parseInt(parts[i].substring(parts[i].lastIndexOf(",") + 1));
            }
            InputRead.close();

        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

    }

    public void setTopThree(){
        int temp;
        for(int i=1; i<highscore.length; i++) {
            for(int j=0; j<highscore.length-i; j++) {
                if(highscore[j]<highscore[j+1]) {
                    temp=highscore[j];
                    highscore[j]=highscore[j+1];
                    highscore[j+1]=temp;
                }

            }
        }


        highscoreList.add("          #1                    " +  highscore[0]);
        highscoreList.add("          #2                    " +  highscore[1]);
        highscoreList.add("          #3                    " +  highscore[2]);
        highscoreList.add("          #4                    " +  highscore[3]);
        highscoreList.add("          #5                    " +  highscore[4]);
    }
}
