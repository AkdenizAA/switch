package com.example.crazy.switch_beleg_v2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;


public class GameScreen  extends Activity implements Animation.AnimationListener {


    Animation animMove;
    Animation animRotate;
    Animation animRotate2;
    ImageView bleenCatcher;
    ImageView blueBleenTop;
    ImageView greenBleenTop;
    TextView score;
    int rotateCounter = 0;
    Intent gameover;
    int highscoreCounter = 0;
    boolean animationDone = true;
    boolean catcherTopBlue = true;
    boolean blueBall = false;
    boolean animationEnd = true;
    //http://www.androidhive.info/2013/06/android-working-with-xml-animations/#rotate

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_screen);

        animMove = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.move);
        animRotate = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.rotate);
        animRotate2 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.rotate2);
        animMove.setAnimationListener(this);
        animRotate.setAnimationListener(this);
        animRotate2.setAnimationListener(this);
        bleenCatcher = (ImageView) findViewById(R.id.animatedImage);
        blueBleenTop  = (ImageView) findViewById(R.id.blueTop);
        greenBleenTop  = (ImageView) findViewById(R.id.greenTop);
        score = (TextView) findViewById(R.id.textScore);

        blueBleenTop.setVisibility(View.GONE);
        greenBleenTop.setVisibility(View.GONE);

        System.out.println("Creating Ball in onCreate");
        createBall();
    }


    public boolean onTouchEvent(MotionEvent e) {
        int eventaction = e.getAction();
        switch (eventaction) {
            case MotionEvent.ACTION_DOWN:
                if(animationDone) {
                    if (rotateCounter % 2 == 0) {
                        animRotate.setFillAfter(true);
                        bleenCatcher.startAnimation(animRotate);
                        rotateCounter++;
                    } else {
                        animRotate2.setFillAfter(true);
                        bleenCatcher.startAnimation(animRotate2);
                        rotateCounter++;
                    }
                }
                break;
        }
        return true;
    }

    @Override
    public void onAnimationEnd(Animation animation) {

        if(animation == animRotate || animation == animRotate2){
            animationDone = true;
            if(catcherTopBlue){
                catcherTopBlue = false;
            }else{
                catcherTopBlue = true;
            }
        }
        if (animation == animMove) {
            checkKollision();
        }
    }

    public void checkKollision(){
        if(animationEnd){
            animationEnd = false;
            System.out.println("Animation Move");
            blueBleenTop.clearAnimation();
            greenBleenTop.clearAnimation();

            blueBleenTop.setVisibility(View.GONE);
            greenBleenTop.setVisibility(View.GONE);

            if(animationDone){
                if(blueBall){
                    //Blauer Ball
                    if(catcherTopBlue){
                        highscoreCounter++;
                        createBall();
                        return;
                    }
                }else{
                    //Gruener Ball
                    if(!catcherTopBlue){
                        highscoreCounter++;
                        createBall();
                        return;
                    }
                }
            }
            System.out.println("You lost: Animation not done.");
            gameover = new Intent(GameScreen.this, GameOver.class);
            gameover.putExtra("Score", highscoreCounter);
            startActivity(gameover);
        }
        score.setText("" + highscoreCounter);
    }

    @Override
    public void onAnimationRepeat(Animation animation) { }

    @Override
    public void onAnimationStart(Animation animation) {
        if(animation == animRotate || animation == animRotate2){
            animationDone = false;
        }
    }

    public void createBall() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                animationEnd = true;
                double random = Math.random() * 100;
                if (random < 51) {
                    System.out.println("Ball: Blue");
                    blueBleenTop.setVisibility(View.VISIBLE);
                    blueBleenTop.startAnimation(animMove);
                    blueBall = true;

                } else {
                    System.out.println("Ball: Green");
                    greenBleenTop.setVisibility(View.VISIBLE);
                    greenBleenTop.startAnimation(animMove);
                    blueBall = false;
                }

            }
        }, 500);
    }


}
