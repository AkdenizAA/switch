package com.example.crazy.switch_beleg_v2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;


public class GameOver extends ActionBarActivity {

    Button game;
    Button hscore;
    TextView textScore;
    TextView textHighscore;
    int score = 0;
    int highscore = 0;
    File scorefile;
    int [] highscore_list = new int[10];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);


        game = (Button) (findViewById(R.id.game));
        game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GameOver.this, GameScreen.class));
            }
        });

        textScore = (TextView) findViewById(R.id.textScore);
        textHighscore = (TextView) findViewById(R.id.textHighscore);

        hscore = (Button)(findViewById(R.id.hscore));
        hscore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GameOver.this, HighScoreScreen.class));
            }
        });
        Bundle extras = getIntent().getExtras();
        score = extras.getInt("Score");

        textScore.setText("Score: " + score);


        saveData();
        loadingData();
        textHighscore.setText("Highscore: " + highscore);
    }


    public void saveData() {
       try {
            FileOutputStream fileout=openFileOutput("highscore5.txt", MODE_APPEND);
            OutputStreamWriter outputWriter=new OutputStreamWriter(fileout);

            outputWriter.write("\r\n");
            outputWriter.write("user," + score);
            outputWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void loadingData() {

    try {
        FileInputStream fileIn = openFileInput("highscore5.txt");
        InputStreamReader InputRead = new InputStreamReader(fileIn);

        char[] inputBuffer = new char[1024];
        int charRead;

        charRead = InputRead.read(inputBuffer) ;

        String readstring = String.copyValueOf(inputBuffer, 0, charRead);
        String[] parts = readstring.split("\r\n");
        for(int i = 1; i < parts.length; i++){
            int temp = Integer.parseInt(parts[i].substring(parts[i].lastIndexOf(",") + 1));

            if(temp > highscore){
                highscore = temp;
            }
        }
        InputRead.close();

    } catch (FileNotFoundException e1) {
        e1.printStackTrace();
    } catch (IOException e1) {
        e1.printStackTrace();
    }

}

}